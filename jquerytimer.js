$( document ).ready(function() {
  var h = 0;
  var m = 0;
  var s = 0;

  var boolean = true;
  var time;

  function addZeroBeforeNumber(nb)
  {
    if(nb < 10)
    {
      nb = "0"+nb;
    }
    return nb;
  }


  $("#timer-start").click(function()
  {


    if(boolean)
    {
      time = setInterval(function()
      {
        s++;

        if(s > 59)
        {
          m++;
          s = 0;
        }

        if(m > 59)
        {
          h++;
          m = 0;
        }


        $("#timer-s").html(addZeroBeforeNumber(s));


        $("#timer-m").html(addZeroBeforeNumber(m));


        $("#timer-h").html(addZeroBeforeNumber(h));


      },1000);

      boolean = false;
    }


  });


  $("#timer-pause").click(function()
  {

    clearInterval(time);


    $("#timer-s").html(addZeroBeforeNumber(s));
    $("#timer-m").html(addZeroBeforeNumber(m));
    $("#timer-h").html(addZeroBeforeNumber(h));


    boolean = true
  });


  $("#timer-reset").click(function()
  {

    s = 0;
    m = 0;
    h = 0;

    $("#timer-s").html("00");
    $("#timer-m").html("00");
    $("#timer-h").html("00");

    if(boolean == false)
    {
      clearInterval(time);
    }

    boolean = true;

  });
});
